import { rest } from 'msw';
import { testData } from './force-directed-map/fakedata';

export const handlers = [
  rest.get('/topology', (req, res, ctx) => {
    const NumOfNodes = parseInt(req.url.searchParams.get('nodes'), 10);
    const NumOfLinks = parseInt(req.url.searchParams.get('links'), 10);
  }),

  rest.get('/data', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(testData));
  }),

  /**
   * Return Node detail information.
   * TODO: Reture fake node detail information.
   */
  rest.get('/node_detail', (req, res, ctx) => {
    const id = req.url.searchParams.get('id');

    if (id.indexOf('sw')) {
      // switch
      console.log(id);
    } else if (id.indexOf('sys')) {
      // compute
      console.log(id);
    } else if (id.indexOf('jbof')) {
      // jbof storage target
      console.log(id);
    } else if (id.indexOf('nvme')) {
      // nvme target
      console.log(id);
    } else if (id.indexOf('iscsi')) {
      // iscsi target
      console.log(id);
    } else if (id.indexOf('1u')) {
      // 1U appliance
      console.log(id);
    } else {
      return res(
        ctx.status(401),
        ctx.json({ result: 'Failed', reason: 'identity error' })
      );
    }
    return res(
      ctx.status(200),
      ctx.json({ result: 'Success', reason: 'success' })
    );
  }),
];
