import {
  ISwitch,
  ICompute,
  I1UAppliance,
  IiSCSITarget,
  INVMeofTarget,
  IJBoF,
} from './force-directed-map/fakeNodeDetail';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRelation } from './force-directed-map/fakedata';

@Injectable({
  providedIn: 'root',
})
export class GetDataService {
  constructor(private http: HttpClient) {}

  /**
   * Get fake nodes and relation link json data
   * @param nodes Integer, How many nodes we want to fake them.
   * @param links Interger, How many links we want.
   */
  getData(nodes: number = 10, links: number = 10): Observable<IRelation> {
    // return this.http.get<IRelation>(`./topology?nodes=${nodes}&links=${links}`);
    return this.http.get<IRelation>(`./data`);
  }

  getNodeDetail(
    identity: string
  ): Observable<
    ISwitch | ICompute | IJBoF | I1UAppliance | IiSCSITarget | INVMeofTarget
  > {
    return this.http.get<
      ISwitch | ICompute | IJBoF | I1UAppliance | IiSCSITarget | INVMeofTarget
    >(`./node_detail?id=${identity}`);
  }
}
