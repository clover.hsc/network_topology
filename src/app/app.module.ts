import { NzSliderModule } from 'ng-zorro-antd/slider';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ForcedirectedmapComponent } from './force-directed-map/forcedirectedmap.component';
import { NgZorroAntdModule, NZ_ICONS } from 'ng-zorro-antd';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NzGridModule } from 'ng-zorro-antd/grid';

@NgModule({
  declarations: [AppComponent, ForcedirectedmapComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    BrowserAnimationsModule,
    NzSliderModule,
    ScrollingModule,
    DragDropModule,
    NzGridModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
