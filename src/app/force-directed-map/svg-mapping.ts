export const NodeIconMapping = {
  // Non-Status icon (only type)
  '1u appliance type': 'ic_1uAppliance_Type.svg',
  'mgmt switch type': 'ic_MgmtSwitch_Warning-3.svg',
  'compute switch type': 'ic_ComputeSwitch_Type.svg',
  'storage switch type': 'ic_StorageSwitch_Type.svg',
  'jbof nvme storage type': 'ic_JBoF_Type.svg',
  'compute type': 'ic_Compute_TypeColor.svg',
  'iscsi target type': 'ic_Compute_ISCSI_Type.svg',
  'nvme target type': 'ic_Compute_NVMeoF_Type.svg',

  // Flash Non-Status icon (only type)
  '1u appliance type flash': 'ic_1uAppliance_Type_Flash.svg',
  'mgmt switch type flash': 'ic_MgmtSwitch_Warning_Flash-3.svg',
  'compute switch type flash': 'ic_ComputeSwitch_Type_Flash.svg',
  'storage switch type flash': 'ic_StorageSwitch_Type_Flash.svg',
  'jbof nvme storage type flash': 'ic_JBoF_Type_Flash.svg',
  'compute type flash': 'ic_Compute_TypeColor_Flash.svg',
  'iscsi target type flash': 'ic_Compute_ISCSI_Type_Flash.svg',
  'nvme target type flash': 'ic_Compute_NVMeoF_Type_Flash.svg',

  // Stale status icon
  '1u appliance stale': 'ic_1uAppliance_Stale.svg',
  '1u appliance stale type': 'ic_1uAppliance_TypeStale.svg',
  'compute stale': 'ic_Compute_Stale.svg',
  'compute stale type': 'ic_Compute_TypeStale.svg',
  'iscsi target stale': 'ic_Compute_ISCSI_Stale.svg',
  'iscsi target stale type': 'ic_Compute_ISCSI_TypeStale.svg',
  'nvme target stale': 'ic_Compute_NVMeoF_Stale.svg',
  'nvme target stale type': 'ic_Compute_NVMeoF_TypeStale.svg',
  'compute switch stale': 'ic_ComputeSwitch_Stale.svg',
  'compute switch stale type': 'ic_ComputeSwitch_TypeStale.svg',
  'mgmt switch stale': 'ic_MgmtSwitch_Warning-4.svg',
  'mgmt switch stale type': 'ic_MgmtSwitch_Warning-5.svg',
  'storage switch stale': 'ic_StorageSwitch_Stale.svg',
  'storage switch stale type': 'ic_StorageSwitch_TypeStale.svg',
  'jbof nvme storage stale': 'ic_JBoF_Stale.svg',
  'jbof nvme storage stale type': 'ic_JBoF_TypeStale.svg',

  // Hide Stale status icon
  '1u appliance stale hide': 'ic_1uAppliance_Stale_Hide.svg',
  '1u appliance stale type hide': 'ic_1uAppliance_TypeStale_Hide.svg',
  'compute stale hide': 'ic_Compute_Stale_Hide.svg',
  'compute stale type hide': 'ic_Compute_TypeStale_Hide.svg',
  'iscsi target stale hide': 'ic_Compute_ISCSI_Stale_Hide.svg',
  'iscsi target stale type hide': 'ic_Compute_ISCSI_TypeStale_Hide.svg',
  'nvme target stale hide': 'ic_Compute_NVMeoF_Stale_hide.svg',
  'nvme target stale type hide': 'ic_Compute_NVMeoF_TypeStale_Hide.svg',
  'compute switch stale hide': 'ic_ComputeSwitch_Stale_Hide.svg',
  'compute switch stale type hide': 'ic_ComputeSwitch_TypeStale_Hide.svg',
  'mgmt switch stale hide': 'ic_MgmtSwitch_Warning_Hide-4.svg',
  'mgmt switch stale type hide': 'ic_MgmtSwitch_Warning_Hide-5.svg',
  'storage switch stale hide': 'ic_StorageSwitch_Stale_Hide.svg',
  'storage switch stale type hide': 'ic_StorageSwitch_TypeStale_Hide.svg',
  'jbof nvme storage stale hide': 'ic_JBoF_Stale_Hide.svg',
  'jbof nvme storage stale type hide': 'ic_JBoF_TypeStale_Hide.svg',

  // Flash Stale status icon
  '1u appliance stale flase': 'ic_1uAppliance_Stale_Flash.svg',
  '1u appliance stale type flash': 'ic_1uAppliance_TypeStale_Flash.svg',
  'compute stale flash': 'ic_Compute_Stale_Flash.svg',
  'compute stale type flash': 'ic_Compute_TypeStale_Flash.svg',
  'iscsi target stale flash': 'ic_Compute_ISCSI_Stale_Flash.svg',
  'iscsi target stale type flash': 'ic_Compute_ISCSI_TypeStale_Flash.svg',
  'nvme target stale flash': 'ic_Compute_NVMeoF_Stale_Flash.svg',
  'nvme target stale type flash': 'ic_Compute_NVMeoF_TypeStale_Flash.svg',
  'compute switch stale flash': 'ic_ComputeSwitch_Stale_Flash.svg',
  'compute switch stale type flash': 'ic_ComputeSwitch_TypeStale_Flash.svg',
  'mgmt switch stale flash': 'ic_MgmtSwitch_Warning_Flash-4.svg',
  'mgmt switch stale type flash': 'ic_MgmtSwitch_Warning_Flash-5.svg',
  'storage switch stale flash': 'ic_StorageSwitch_Stale_Flash.svg',
  'storage switch stale type flash': 'ic_StorageSwitch_TypeStale_Flash.svg',
  'jbof nvme storage stale flash': 'ic_JBoF_Stale_Flash.svg',
  'jbof nvme storage stale type flash': 'ic_JBoF_TypeStale_Flash.svg',

  // Health status icon
  '1u appliance good': 'ic_1uAppliance_Good.svg',
  '1u appliance warning': 'ic_1uAppliance_Warning.svg',
  '1u appliance critical': 'ic_1uAppliance_Critical.svg',
  'mgmt switch good': 'ic_MgmtSwitch_Good.svg',
  'mgmt switch warning': 'ic_MgmtSwitch_Warning.svg',
  'mgmt switch critical': 'ic_MgmtSwitch_Warning-1.svg',
  'compute switch good': 'ic_ComputeSwitch_Good.svg',
  'compute switch warning': 'ic_ComputeSwitch_Warning.svg',
  'compute switch critical': 'ic_Compute_HealthCritical.svg',
  'storage switch good': 'ic_StorageSwitch_Good.svg',
  'storage switch warning': 'ic_StorageSwitch_Warning.svg',
  'storage switch critical': 'ic_StorageSwitch_Critical.svg',
  'compute good': 'ic_Compute_HealthGood.svg',
  'compute warning': 'ic_Compute_HealthWarning.svg',
  'compute critical': 'ic_Compute_HealthCritical.svg',
  'jbof nvme storage good': 'ic_JBoF_Good.svg',
  'jbof nvme storage warning': 'ic_JBoF_Warning.svg',
  'jbof nvme storage critical': 'ic_JBoF_Critical.svg',
  'iscsi target good': 'ic_Compute_ISCSI_Good.svg',
  'iscsi target warning': 'ic_Compute_ISCSI_Warning.svg',
  'iscsi target critical': 'ic_Compute_ISCSI_Critical.svg',
  'nvme target good': 'ic_Compute_NVMeoF_Good.svg',
  'nvme target warning': 'ic_Compute_NVMeoF_Warning.svg',
  'nvme target critical': 'ic_Compute_NVMeoF_Critical.svg',

  // Hide status icon
  '1u appliance good hide': 'ic_1uAppliance_Good_Hide.svg',
  '1u appliance warning hide': 'ic_1uAppliance_Warning_Hide.svg',
  '1u appliance critical hide': 'ic_1uAppliance_Critical_Hide.svg',
  'mgmt switch good hide': 'ic_MgmtSwitch_Good_Hide.svg',
  'mgmt switch warning hide': 'ic_MgmtSwitch_Warning_Hide.svg',
  'mgmt switch critical hide': 'ic_MgmtSwitch_Warning_Hide-1.svg',
  'compute switch good hide': 'ic_ComputeSwitch_Good_Hide.svg',
  'compute switch warning hide': 'ic_ComputeSwitch_Warning_Hide.svg',
  'compute switch critical hide': 'ic_Compute_HealthCritical_Hide.svg',
  'storage switch good hide': 'ic_StorageSwitch_Good_Hide.svg',
  'storage switch warning hide': 'ic_StorageSwitch_Warning_Hide.svg',
  'storage switch critical hide': 'ic_StorageSwitch_Critical_Hide.svg',
  'compute good hide': 'ic_Compute_HealthGood_Hide.svg',
  'compute warning hide': 'ic_Compute_HealthWarning_Hide.svg',
  'compute critical hide': 'ic_Compute_HealthCritical_Hide.svg',
  'jbof nvme storage good hide': 'ic_JBoF_Good_Hide.svg',
  'jbof nvme storage warning hide': 'ic_JBoF_Warning_Hide.svg',
  'jbof nvme storage critical hide': 'ic_JBoF_Critical_Hide.svg',
  'iscsi target good hide': 'ic_Compute_ISCSI_Good_Hide.svg',
  'iscsi target warning hide': 'ic_Compute_ISCSI_Warning_Hide.svg',
  'iscsi target critical hide': 'ic_Compute_ISCSI_Critical_Hide.svg',
  'nvme target good hide': 'ic_Compute_NVMeoF_Good_Hide.svg',
  'nvme target warning hide': 'ic_Compute_NVMeoF_Warning_Hide.svg',
  'nvme target critical hide': 'ic_Compute_NVMeoF_Critical_Hide.svg',

  // Flash Status icon
  '1u appliance good flash': 'ic_1uAppliance_Good_Flash.svg',
  '1u appliance warning flash': 'ic_1uAppliance_Warning_Flash.svg',
  '1u appliance critical flash': 'ic_1uAppliance_Critical_Flash.svg',
  'mgmt switch good flash': 'ic_MgmtSwitch_Good_Flash.svg',
  'mgmt switch warning flash': 'ic_MgmtSwitch_Warning_Flash.svg',
  'mgmt switch critical flash': 'ic_MgmtSwitch_Warning_Flash-1.svg',
  'compute switch good flash': 'ic_ComputeSwitch_Good_Flash.svg',
  'compute switch warning flash': 'ic_ComputeSwitch_Warning_Flash.svg',
  'compute switch critical flash': 'ic_Compute_HealthCritical_Flash.svg',
  'storage switch good flash': 'ic_StorageSwitch_Good_Flash.svg',
  'storage switch warning flash': 'ic_StorageSwitch_Warning_Flash.svg',
  'storage switch critical flash': 'ic_StorageSwitch_Critical_Flash.svg',
  'compute good flash': 'ic_Compute_HealthGood_Flash.svg',
  'compute warning flash': 'ic_Compute_HealthWarning_Flash.svg',
  'compute critical flash': 'ic_Compute_HealthCritical_Flash.svg',
  'jbof nvme storage good flash': 'ic_JBoF_Good_Flash.svg',
  'jbof nvme storage warning flash': 'ic_JBoF_Warning_Flash.svg',
  'jbof nvme storage critical flash': 'ic_JBoF_Critical_Flash.svg',
  'iscsi target good flash': 'ic_Compute_ISCSI_Good_Flash.svg',
  'iscsi target warning flash': 'ic_Compute_ISCSI_Warning_Flash.svg',
  'iscsi target critical flash': 'ic_Compute_ISCSI_Critical_Flash.svg',
  'nvme target good flash': 'ic_Compute_NVMeoF_Good_Flash.svg',
  'nvme target warning flash': 'ic_Compute_NVMeoF_Warning_Flash.svg',
  'nvme target critical flash': 'ic_Compute_NVMeoF_Critical_Flash.svg',

  // power off icon
  '1u appliance off': 'ic_1uappliance_PowerOff.svg',
  'mgmt switch off': 'ic_MgmtSwitch_Warning-2.svg',
  'compute switch off': 'ic_ComputeSwitch_PowerOff.svg',
  'storage switch off': 'ic_StorageSwitch_PowerOff.svg',
  'compute off': 'ic_Compute_PowerOff.svg',
  'jbof nvme storage off': 'ic_JBoF_PowerOff.svg',
  'iscsi target off': 'ic_Compute_ISCSI_PowerOff.svg',
  'nvme target off': 'ic_Compute_NVMeoF_PowerOff.svg',

  // Hide Power off icon
  '1u appliance off hide': 'ic_1uappliance_PowerOff_Hide.svg',
  'mgmt switch off hide': 'ic_MgmtSwitch_Warning_Hide-2.svg',
  'compute switch off hide': 'ic_ComputeSwitch_PowerOff_Hide.svg',
  'storage switch off hide': 'ic_StorageSwitch_PowerOff_Hide.svg',
  'compute off hide': 'ic_Compute_PowerOff_Hide.svg',
  'jbof nvme storage off hide': 'ic_JBoF_PowerOff_Hide.svg',
  'iscsi target off hide': 'ic_Compute_ISCSI_PowerOff_Hide.svg',
  'nvme target off hide': 'ic_Compute_NVMeoF_PowerOff_Hide.svg',

  // Flash Power off icon
  '1u appliance off flash': 'ic_1uappliance_PowerOff_Flash.svg',
  'mgmt switch off flash': 'ic_MgmtSwitch_Warning_Flash-2.svg',
  'compute switch off flash': 'ic_ComputeSwitch_PowerOff_Flash.svg',
  'storage switch off flash': 'ic_StorageSwitch_PowerOff_Flash.svg',
  'compute off flash': 'ic_Compute_PowerOff_Flash.svg',
  'jbof nvme storage off flash': 'ic_JBoF_PowerOff_Flash.svg',
  'iscsi target off flash': 'ic_Compute_ISCSI_PowerOff_Flash.svg',
  'nvme target off flash': 'ic_Compute_NVMeoF_PowerOff_Flash.svg',
};
