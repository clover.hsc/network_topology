import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForcedirectedmapComponent } from './forcedirectedmap.component';

describe('ForcedirectedmapComponent', () => {
  let component: ForcedirectedmapComponent;
  let fixture: ComponentFixture<ForcedirectedmapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ForcedirectedmapComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForcedirectedmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
