export interface ISwitch {
  name: string;
  ip: string;
  location: string;
  sku: string;
  mac: string;
  health: string;
  port_counter: number;
}

export interface ICompute {
  name: string;
  ip: string;
  location: string;
  sku: string;
  manufacturer: string;
  power_state: string;
  health: string;
  lan: Array<string>;
}

export interface IJBoF {
  name: string;
  ip: string;
  location: string;
  sku: string;
  manufacturer: string;
  power_state: string;
  health: string;
  lan: Array<string>;
}

export interface I1UAppliance {
  host_name: string;
  lldp_chassis_id: string;
  ha_role?: string;
  network: string;
  network_detail: Array<InetworkInfo>;
}

interface InetworkInfo {
  port_name: string;
  ip: string;
}

export interface IiSCSITarget {
  name: string;
  ip: string;
  location: string;
  sku: string;
  manufacturer: string;
  power_state: string;
  health: string;
  lan: Array<string>;
  lan_detail: Array<IlanInfo>;
}

interface IlanInfo {
  name: string;
  mac: string;
  speed: string;
  submas: string;
}

export interface INVMeofTarget {
  name: string;
  ip: string;
  location: string;
  sku: string;
  manufacturer: string;
  power_state: string;
  health: string;
  lan: Array<string>;
  lan_detail: Array<IlanInfo>;
}
