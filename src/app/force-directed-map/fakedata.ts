// tslint:disable: no-redundant-jsdoc

import * as faker from 'faker';
import { internet } from 'faker';

// Nodes attribute, A topology only exist one 1U Appliance and Mgmt Switch.
const NodeName = [
  'Mgmt Switch',
  'Compute Switch',
  'Storage Switch',
  'NVMe Target',
  'ISCSi Target',
  'Compute',
  'JBoF NVMe Storage',
  '1U Appliance',
];

const NodeType = ['Switch', 'Compute', 'NVMe', 'ISCSi', '1U', 'JBoF'];

const PowerStatus = ['on', 'off'];
const Stale = [true, false];
const NodeHealth = ['Good', 'Warning', 'Critical'];

// link attribute
const LinkStatus = ['up', 'down', 'bmc broken', 'lan1 broken'];
const LinkType = ['mgmt', 'common'];

/**
 * @pmember health : Good(green), Wrning(yellow), Critical(red)
 * @member powerStatus : off(gray), on(check health)
 * @member stale : If true ! with question mark node icon and gray color
 */
export interface INode {
  name: string;
  ip: string;
  location: string;
  type: string;
  health: string;
  powerStatus: string;
  stale: boolean;
  mac: string;
  uuid: string;
  totalPorts: number;
}

/**
 * When switch connect to switch, we will obtain the 2 Links
 * with the same contents but they with opposite source and target
 * node.
 * @member source : Switch node
 * @member target : node
 * @member Status : up(green), off(gray), bmc broken(red)
 * @member type : mgmt(dotted line), common(solid line)
 * @member portLinkList : Object, each connection detail information
 * @member numberOfLines : length of portLinkList, Count by Front-end.
 */
export interface ILink {
  source: string;
  target: string;
  status: string;
  type: 'mgmt' | 'common';
  portLinkList: Array<IPortLink>;
  numberOfLines: number;
}

export interface IPortLink {
  // Source always be switch.
  source: {
    portName: string;
    macAddr: string;
    lan?: string;
    ip: string;
  };

  // Target: port information.
  target: {
    portName?: string;
    macAddr: string;
    lan?: string;
    ip: string;
  };

  // 'up', 'down', 'bmc broken' or 'lan1 broken'.
  status: string;

  // 'mgmt' or 'common'
  type: string;
  bmcPort: boolean;
}

export interface IRelation {
  linkList: Array<ILink>;
  nodeList: Array<INode>;
  result: string;
}

export const testData = {
  linkList: [
    {
      source: '192.168.1.11',
      target: '192.168.1.14',
      status: '', // front-end needs to calculate.
      type: 'common',
      portLinkList: [
        {
          source: {
            portName: 'eno3', // if name is start with 'mgmt' --> mgmt switch.
            macAddr: '',
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi4',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: false,
        },
      ],
      numberOfLines: undefined, // front-end needs to calculate.
    },
    {
      source: '192.168.1.13',
      target: '192.168.1.14',
      status: '',
      type: 'common',
      portLinkList: [
        {
          source: {
            portName: 'gi5',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: '',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: false,
        },
        {
          source: {
            portName: 'gi6',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: '',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'down',
          type: 'common',
          bmcPort: false,
        },
      ],
      numberOfLines: undefined,
    },
    {
      source: '192.168.1.15',
      target: '192.168.1.14',
      status: '',
      type: 'common',
      portLinkList: [
        {
          source: {
            portName: 'gi1',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi1',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: false,
        },
        {
          source: {
            portName: 'gi2',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi2',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'down',
          type: 'common',
          bmcPort: false,
        },
        {
          source: {
            portName: 'gi3',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi3',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: false,
        },
      ],
      numberOfLines: undefined,
    },
    {
      source: '192.168.1.14',
      target: '192.168.1.15',
      status: '',
      type: 'common',
      portLinkList: [
        {
          source: {
            portName: 'gi1',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi1',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: false,
        },
        {
          source: {
            portName: 'gi2',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi2',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'down',
          type: 'common',
          bmcPort: false,
        },
        {
          source: {
            portName: 'gi3',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi3',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: false,
        },
      ],
      numberOfLines: undefined,
    },
    {
      source: '192.168.1.14',
      target: '192.168.1.12',
      status: '',
      type: 'mgmt',
      portLinkList: [
        {
          source: {
            portName: 'gi23',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: '',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'off',
          type: 'common',
          bmcPort: false,
        },
      ],
      numberOfLines: undefined,
    },
    {
      source: '192.168.1.14',
      target: '192.168.1.16',
      status: '',
      type: 'common',
      portLinkList: [
        {
          source: {
            portName: 'gi1',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: 'gi1',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'bmc broken',
          type: 'common',
          bmcPort: true,
        },
      ],
      numberOfLines: undefined,
    },
    {
      source: '192.168.1.15',
      target: '192.168.1.17',
      status: '',
      type: 'common',
      portLinkList: [
        {
          source: {
            portName: 'gi10',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: '',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: true,
        },
        {
          source: {
            portName: 'gi11',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          target: {
            portName: '',
            macAddr: faker.internet.mac(),
            lan: '',
            ip: faker.internet.ip(),
          },
          status: 'up',
          type: 'common',
          bmcPort: true,
        },
      ],
      numberOfLines: undefined,
    },
  ],
  nodeList: [
    {
      name: '1U Appliance',
      ip: '192.168.1.11',
      location: '',
      type: 'Switch',
      health: 'Good',
      powerStatus: 'on',
      stale: false,
      mac: faker.internet.mac(),
      uuid: faker.random.uuid(),
      totalPorts: 4,
    },
    {
      name: 'JBoF NVMe Storage',
      ip: '192.168.1.12',
      location: 'R_1:17-1',
      type: 'JBoF',
      health: '',
      powerStatus: 'on',
      stale: true,
      mac: faker.internet.mac(),
      uuid: faker.random.uuid(),
      totalPorts: 4,
    },
    {
      name: 'Compute',
      ip: '192.168.1.13',
      location: 'R_1:30-1',
      type: 'Compute',
      health: '',
      powerStatus: 'off',
      stale: false,
      mac: faker.internet.mac(),
      uuid: faker.random.uuid(),
      totalPorts: 4,
    },
    {
      name: 'Mgmt Switch',
      ip: '192.168.1.14',
      location: 'R_23:93',
      type: 'Switch',
      health: 'Good',
      powerStatus: 'on',
      stale: false,
      mac: faker.internet.mac(),
      uuid: faker.random.uuid(),
      totalPorts: 52,
    },
    {
      name: 'Compute Switch',
      ip: '192.168.1.15',
      location: 'R_23:94',
      type: 'Switch',
      health: 'Good',
      powerStatus: 'on',
      stale: false,
      mac: faker.internet.mac(),
      uuid: faker.random.uuid(),
      totalPorts: 48,
    },
    {
      name: 'NVMe Target',
      ip: '192.168.1.16',
      location: 'R_23:95',
      type: 'Compute',
      health: 'Warning',
      powerStatus: 'on',
      stale: false,
      mac: faker.internet.mac(),
      uuid: faker.random.uuid(),
      totalPorts: 4,
    },
    {
      name: 'Compute',
      ip: '192.168.1.17',
      location: 'R_23:96',
      type: 'Compute',
      health: 'Critical',
      powerStatus: 'on',
      stale: false,
      mac: faker.internet.mac(),
      uuid: faker.random.uuid(),
      totalPorts: 4,
    },
  ],
  result: 'Success',
};
