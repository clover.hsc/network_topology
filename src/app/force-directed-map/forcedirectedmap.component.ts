import { NodeIconMapping } from './svg-mapping';
import { GetDataService } from './../get-data.service';
import { cloneDeep } from 'lodash';
import {
  Component,
  OnInit,
  ViewEncapsulation,
  ElementRef,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import * as d3 from 'd3';

import { IRelation, INode, ILink } from './fakedata';
import {
  ISwitch,
  ICompute,
  I1UAppliance,
  IiSCSITarget,
  INVMeofTarget,
  IJBoF,
} from './fakeNodeDetail';

@Component({
  selector: 'app-trycitymap',
  templateUrl: './forcedirectedmap.component.html',
  styleUrls: ['./forcedirectedmap.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ForcedirectedmapComponent implements OnInit {
  svg: any;
  simulation: any;
  simulationBodyStrength = -5000;

  zoomValue = 1;
  zoom: any;

  iconWidth = 50;
  iconHeight = 50;
  ipAddrPaddingTop = 15;

  topologydata: IRelation;
  topologySectionWidth = 1589;
  topologySectionHeight = 834;

  link: any;
  node: any;
  ipAddr: any;

  linkLineWidth = 3;
  numOfConnectionCircleRadius = 12;
  numOfConnectionCircle: any;
  numOfConnectionText: any;

  nodeTip: any;
  nodeTipData: INode;
  nodeTipWidth = 125;
  nodeTipHeight = 122;
  lineTip: any;

  handlerFocusNodeFlash: any;

  form: FormGroup;

  ExpandNodeInfor:
    | ISwitch
    | ICompute
    | I1UAppliance
    | IiSCSITarget
    | INVMeofTarget
    | IJBoF;

  constructor(
    private getDataServ: GetDataService,
    private formBuilder: FormBuilder,
    private element: ElementRef
  ) {
    this.form = this.formBuilder.group({
      host: [true],
      healthGood: [true],
      healthWarning: [true],
      healthCritical: [true],
      powerOff: [true],
      stale: [true],
      linkState: [true],
      up: [true],
      down: [true],
      bmcBroken: [true],
      ipaddr: [true],
    });
  }

  ngOnInit(): void {
    this.nodeTip = d3.select('#node-tip');
    this.lineTip = d3.select('#line-tip');

    this.InitialRenderSetion();
    this.initialZoomBehavior();
    this.InitialSimulation();
    this.getDataServ.getData(5, 4).subscribe(
      (res: IRelation) => {
        this.topologydata = this.UpdateLineAttribute(res);

        this.renderLine(this.topologydata.linkList);
        this.renderNumOfConnectionCircle(this.topologydata.linkList);
        this.renderNumOfConnection(this.topologydata.linkList);
        this.RenderNodes(this.topologydata.nodeList);
        this.RenderNodeIPAddr(this.topologydata.nodeList);

        this.simulation
          .nodes(this.topologydata.nodeList)
          .on('tick', this.ticked.bind(this));
        this.simulation.force('link').links(this.topologydata.linkList);
      },
      (error) => console.log(error.error.result)
    );
  }

  /**
   * Access the render section and initial the width & height
   */
  InitialRenderSetion(): void {
    d3.select('svg').remove();
    this.svg = d3
      .select('div#network_tp')
      .append('svg')
      .attr('xmlns', 'http://www.w3.org/2000/svg')
      .attr('width', this.topologySectionWidth)
      .attr('height', this.topologySectionHeight)
      .append('g')
      .attr('id', 'element-group');
  }

  /**
   * Create force simulation to prepare for graphy.
   */
  InitialSimulation(): void {
    // Create force simulation to drawer the force directed graphic.
    this.simulation = d3
      .forceSimulation()
      .force(
        'link',
        d3.forceLink().id((d: any) => d.ip)
      )
      // making elements repel one another
      .force('charge', d3.forceManyBody().strength(this.simulationBodyStrength))

      // Cause elements to be attracted towards specified position
      .force(
        'center',
        d3.forceCenter(
          this.topologySectionWidth / 2,
          this.topologySectionHeight / 2
        )
      );
  }

  /**
   * To calculate current line status and numberOfLines value.
   * @param data : Original Topology data
   */
  UpdateLineAttribute(data: IRelation): IRelation {
    const copyData: IRelation = cloneDeep(data);
    let status = '';
    copyData.linkList.forEach((link: ILink) => {
      link.numberOfLines = link.portLinkList.length;

      // line status. Priority : bmc > lan1 > down > up
      if (link.portLinkList.find((portlk) => portlk.status === 'bmc broken')) {
        status = 'bmc broken';
      } else if (
        link.portLinkList.find((portlk) => portlk.status === 'lan1 broken')
      ) {
        status = 'lan1 broken';
      } else if (link.portLinkList.find((portlk) => portlk.status === 'down')) {
        status = 'down';
      } else {
        status = 'up';
      }

      link.status = status;
    });

    // console.log(copyData);
    return copyData;
  }

  RenderNodes(NodeData: Array<INode>): void {
    this.node = this.svg
      .append('g')
      .attr('class', 'nodes')
      .selectAll('image')
      .data(NodeData)
      .enter()
      .append('svg:image')
      .call(this.setNodeIMG)
      .call(this.setAltNodeName)
      .call(this.setNodeStatusClass)
      .call(this.setClickEvent.bind(this))
      .on('mouseover', this.setNodeMouseover.bind(this))
      .on('mouseout', this.setNodeMouseout.bind(this))
      .call(
        d3
          .drag()
          .on('start', this.dragstarted.bind(this))
          .on('drag', this.dragged.bind(this))
          .on('end', this.dragended.bind(this))
      );
  }

  /**
   * Assign the node name with dash into alt attribute.
   * Front-end will reference this node name to change to correct icon when the
   * user checked or un-checked status checkbox options.
   * @param sel : selection object
   */
  setAltNodeName(sel: any): void {
    sel.attr('alt', (node: INode) => node.name.toLocaleLowerCase());
  }

  /**
   * Check node' stale or health to assign the correct image for node.
   * @param sel : selectObj
   */
  setNodeIMG(sel: any): void {
    sel
      .attr('xlink:href', (node: INode) => {
        let mapName = '';
        if (node.stale) {
          mapName = `${node.name.toLocaleLowerCase()} stale`;
        } else if (node.powerStatus.toLocaleLowerCase() === 'off') {
          mapName = `${node.name.toLocaleLowerCase()} off`;
        } else if (node.health.toLocaleLowerCase() === 'critical') {
          mapName = `${node.name.toLocaleLowerCase()} critical`;
        } else if (node.health.toLocaleLowerCase() === 'warning') {
          mapName = `${node.name.toLocaleLowerCase()} warning`;
        } else {
          mapName = `${node.name.toLocaleLowerCase()} good`;
        }
        return `assets/${NodeIconMapping[mapName]}`;
      })
      .classed('type', false);
  }

  /**
   * When mouse over on the node then to show the tooltip on the node.
   * @param d : MouseEvent.
   * @param node : Node data.
   */
  setNodeMouseover(d: any, node: any): void {
    this.nodeTipData = node; // for render data on the Tip.
    const $ip = this.element.nativeElement.querySelector('#ipaddr-tip');

    // remove all exist class anme
    [...$ip.classList].forEach((classname) => {
      $ip.classList.remove(classname);
    });

    if (d.target.classList.contains('type')) {
      // this node in non-status mode.
      if (d.target.classList.contains('type')) {
        // use non-status style on IP address value.
        $ip.classList.add(`type-${node.type.toLocaleLowerCase()}`);
      }
    } else {
      // assign the node status style to IP address value.
      $ip.classList.add(
        d.target.getAttribute('class').replace('focus', '').trim()
      );
    }
    this.nodeTip
      .transition()
      .duration(0)
      .style('opacity', 1)
      .style('left', `${node.x - this.nodeTipWidth / 2}px`)
      .style('top', `${node.y - this.iconHeight / 2 - this.nodeTipHeight}px`);
  }

  /**
   * After mouse move out the node , hide the node tip.
   * @param d : mouse event.
   * @param node : node data
   */
  setNodeMouseout(d: any, node: INode): void {
    console.log('mouse out');
    this.nodeTip
      .transition()
      .duration(0)
      .style('opacity', 0)
      .style('left', `0px`)
      .style('top', `0px`);
  }

  /**
   * Assign the status in the class memeber to identity node's status.
   * @param sel : selectObj
   */
  setNodeStatusClass(sel: any): void {
    sel
      .classed('stale', (node: INode) => (node.stale ? true : false))
      .classed('off', (node: INode) =>
        node.powerStatus.toLocaleLowerCase() === 'off' ? true : false
      )
      .classed('critical', (node: INode) =>
        node.health.toLocaleLowerCase() === 'critical' ? true : false
      )
      .classed('warning', (node: INode) =>
        node.health.toLocaleLowerCase() === 'warning' ? true : false
      )
      .classed('good', (node: INode) =>
        node.health.toLocaleLowerCase() === 'good' ? true : false
      );
  }

  /**
   * Stop pre-clicked node icon flash and flash the clicked node icon then trigger api
   * to get thhe node detail information show on eexpand the section in the right side.
   * @param sel : Selection object.
   * TODO: trigger api to get node info and show on expand section.
   */
  setClickEvent(sel: any): void {
    sel.on('click', (d, i: INode) => {
      const preClickNode = document.querySelector('.focus');
      let mapName = '';
      let tick = true;

      clearInterval(this.handlerFocusNodeFlash);

      // disable the flash effect on pre-node the user clicked.
      if (preClickNode) {
        // check the node is in type style or not
        if (preClickNode.classList.contains('type')) {
          // roll back to normal type icon
          if (preClickNode.classList.contains('stale')) {
            mapName = `${preClickNode
              .getAttribute('alt')
              .toLocaleLowerCase()} stale type`;
          } else {
            mapName = `${preClickNode
              .getAttribute('alt')
              .toLocaleLowerCase()} type`;
          }
        } else {
          // roll back to normal staus icon
          if (preClickNode.classList.contains('stale')) {
            mapName = `${preClickNode
              .getAttribute('alt')
              .toLocaleLowerCase()} stale`;
          } else if (preClickNode.classList.contains('off')) {
            mapName = `${preClickNode
              .getAttribute('alt')
              .toLocaleLowerCase()} off`;
          } else if (preClickNode.classList.contains('critical')) {
            mapName = `${preClickNode
              .getAttribute('alt')
              .toLocaleLowerCase()} critical`;
          } else if (preClickNode.classList.contains('warning')) {
            mapName = `${preClickNode
              .getAttribute('alt')
              .toLocaleLowerCase()} warning`;
          } else if (preClickNode.classList.contains('good')) {
            mapName = `${preClickNode
              .getAttribute('alt')
              .toLocaleLowerCase()} good`;
          }
        }
        preClickNode.setAttribute('href', `assets/${NodeIconMapping[mapName]}`);
        preClickNode.classList.remove('focus');
      }

      // set the clicked node to with flash effect and suitable icon.
      this.handlerFocusNodeFlash = setInterval(() => {
        d.target.classList.add('focus');
        if (tick) {
          tick = !tick;
          if (d.target.classList.contains('type')) {
            // use type style to flash the icon
            if (d.target.classList.contains('stale')) {
              mapName = `${i.name.toLocaleLowerCase()} stale type flash`;
            } else {
              mapName = `${i.name.toLocaleLowerCase()} type flash`;
            }
          } else {
            // roll back to normal staus icon
            if (d.target.classList.contains('stale')) {
              mapName = `${i.name.toLocaleLowerCase()} stale flash`;
            } else if (d.target.classList.contains('off')) {
              mapName = `${i.name.toLocaleLowerCase()} off flash`;
            } else if (d.target.classList.contains('critical')) {
              mapName = `${i.name.toLocaleLowerCase()} critical flash`;
            } else if (d.target.classList.contains('warning')) {
              mapName = `${i.name.toLocaleLowerCase()} warning flash`;
            } else if (d.target.classList.contains('good')) {
              mapName = `${i.name.toLocaleLowerCase()} good flash`;
            }
          }
          d.target.setAttribute('href', `assets/${NodeIconMapping[mapName]}`);
        } else {
          tick = !tick;
          if (d.target.classList.contains('type')) {
            // use type style to flash the icon
            if (d.target.classList.contains('stale')) {
              mapName = `${i.name.toLocaleLowerCase()} stale type`;
            } else {
              mapName = `${i.name.toLocaleLowerCase()} type`;
            }
          } else {
            // roll back to normal staus icon
            if (d.target.classList.contains('stale')) {
              mapName = `${i.name.toLocaleLowerCase()} stale`;
            } else if (d.target.classList.contains('off')) {
              mapName = `${i.name.toLocaleLowerCase()} off`;
            } else if (d.target.classList.contains('critical')) {
              mapName = `${i.name.toLocaleLowerCase()} critical`;
            } else if (d.target.classList.contains('warning')) {
              mapName = `${i.name.toLocaleLowerCase()} warning`;
            } else if (d.target.classList.contains('good')) {
              mapName = `${i.name.toLocaleLowerCase()} good`;
            }
          }
          d.target.setAttribute('href', `assets/${NodeIconMapping[mapName]}`);
        }
      }, 600);

      this.getNodeDetailInformation(i); // Non implement !
    });
  }

  /**
   * After clicks on the node, Front-end check the node type to
   * integrate identity string then trigger api to get detail information
   * of the node for list on the expand information section in the right side.
   * @param node : Node data object.
   * TODO: list the detail information of node to the expand section in the right side.
   */
  getNodeDetailInformation(node: INode): void {
    let identity = '';
    if (node.type.toLocaleLowerCase() === 'switch') {
      identity = `sw-${node.mac}_${node.ip}`;
    } else if (node.type.toLocaleLowerCase() === 'compute') {
      identity = `sys-${node.uuid}`;
    } else if (node.type.toLocaleLowerCase() === 'jbof') {
      identity = `jbof-${node.uuid}`;
    } else if (node.type.toLocaleLowerCase() === 'nvme') {
      identity = `nvme-${node.uuid}`;
    } else if (node.type.toLocaleLowerCase() === 'iscsi') {
      identity = `iscsi-${node.uuid}`;
    } else {
      // 1U appliance device
      identity = `1u-${node.ip}`;
    }

    this.getDataServ.getNodeDetail(identity).subscribe((resp) => {});
  }

  /**
   * show the ip address under the node.
   * @param NodeData : Array of Node data.
   */
  RenderNodeIPAddr(NodeData: Array<INode>): void {
    this.ipAddr = this.svg
      .append('g')
      .attr('class', 'ip-group')
      .selectAll('text')
      .data(NodeData)
      .enter()
      .append('text')
      .text((node: INode) => node.ip)
      .classed('ip', true);
  }

  renderLine(LineData: Array<ILink>): void {
    this.link = this.svg
      .append('g')
      .attr('class', 'links')
      .selectAll('line')
      .data(LineData)
      .enter()
      .append('line')
      .call(this.setLineType)
      .call(this.colorByLinkStatus)
      .attr('stroke-width', this.linkLineWidth)
      .on('mouseover', (d, i) => console.log('mouseover'));
  }

  /**
   * When mouse over on the link, show the tooltip to on the link
   * @param d : Mouser event.
   * @param line: line data.
   * TODO: Not implement.
   */
  setLinkMouseover(d: any, line: any): void {}

  /**
   * When mouse out on the link, hide the tooltip.
   * @param d : Mouser event.
   * @param line: line data.
   * TODO: Not implement.
   */
  setLinkMouseout(d: any, line: any): void {}

  /**
   * Ref type of line to render the common line or dash line.
   * @param sel : d3 selection object.
   */
  setLineType(sel: any): void {
    sel
      .classed('common-line', (linkType: ILink) =>
        linkType.type.toLocaleLowerCase() === 'common' ? true : false
      )
      .classed('mgmt-line', (linkType: ILink) =>
        linkType.type.toLocaleLowerCase() === 'mgmt' ? true : false
      );
  }

  /**
   * Ref status value of line to draw the color of line.
   * @param sel : d3 selection object.
   */
  colorByLinkStatus(sel: any): void {
    sel
      .classed('up', (linkStatus: ILink) =>
        linkStatus.status.toLocaleLowerCase() === 'up' ? true : false
      )
      .classed('down', (linkStatus: ILink) =>
        linkStatus.status.toLocaleLowerCase() === 'down' ? true : false
      )
      .classed('bmc-broken', (linkStatus: ILink) =>
        linkStatus.status.toLocaleLowerCase() === 'bmc broken' ? true : false
      )
      .classed('lan1-broken', (linkStatus: ILink) =>
        linkStatus.status.toLocaleLowerCase() === 'lan1 broken' ? true : false
      );
  }

  /**
   *  A circle to be a background under the number of connections.
   * @param LineData : Array of Link data
   */
  renderNumOfConnectionCircle(LineData: Array<ILink>): void {
    this.numOfConnectionCircle = this.svg
      .append('g')
      .attr('class', 'num-of-connection-circle')
      .selectAll('circle')
      .data(LineData)
      .enter()
      .append('circle')
      .attr('r', (line: ILink) =>
        line.portLinkList.length > 1 ? this.numOfConnectionCircleRadius : 0
      )
      .call(this.colorByLinkStatus);
  }

  /**
   * To calculate and show the number of connection
   * @param LineData : Array of link data
   */
  renderNumOfConnection(LineData: Array<ILink>): void {
    this.numOfConnectionText = this.svg
      .append('g')
      .attr('class', 'num-of-connection-text')
      .selectAll('text')
      .data(LineData)
      .enter()
      .append('text')
      .text((line: ILink) => {
        if (line.portLinkList.length > 1) {
          return line.portLinkList.length;
        } else {
          return '';
        }
      })
      .classed('num-of-connection', true);
  }

  /**
   * When move node, re-calculate topology elements position.
   */
  ticked(): void {
    this.node
      .attr('x', (d: any) => d.x)
      .attr('y', (d: any) => d.y)
      .attr(
        'transform',
        (d) => `translate(${-this.iconWidth / 2}, ${-this.iconHeight / 2})`
      ); // for svg file

    // this.node.attr('cx', (d: any) => d.x).attr('cy', (d: any) => d.y);
    this.node.exit().remove();

    this.link
      .attr('x1', (d: any) => {
        // console.log(d);
        return d.source.x;
      })
      .attr('y1', (d: any) => d.source.y)
      .attr('x2', (d: any) => d.target.x)
      .attr('y2', (d: any) => d.target.y)
      .exit()
      .remove();

    // Change IP Address location
    this.ipAddr
      .attr('x', (d: any) => d.x)
      .attr('y', (d: any) => d.y + this.iconHeight / 2 + this.ipAddrPaddingTop)
      .exit()
      .remove();

    // cjange circle position
    this.numOfConnectionCircle
      .attr('cx', (d: any) =>
        Math.abs(Math.ceil((d.target.x + d.source.x) / 2))
      )
      .attr('cy', (d: any) =>
        Math.abs(Math.ceil((d.target.y + d.source.y) / 2))
      )
      .exit()
      .remove();

    this.numOfConnectionText
      .attr('dx', (d: any) =>
        Math.abs(Math.ceil((d.target.x + d.source.x) / 2))
      )
      .attr('dy', (d: any) =>
        Math.abs(Math.ceil((d.target.y + d.source.y) / 2) + this.linkLineWidth)
      )
      .exit()
      .remove();
  }

  dragstarted(event: any, d: any): void {
    if (!event.active) {
      this.simulation.alphaTarget(0.1).restart();
    }
    d.fx = d.x;
    d.fy = d.y;
  }

  dragged(event: any, d: any): void {
    d.fx = event.x;
    d.fy = event.y;
  }

  dragended(event: any, d: any): void {
    // console.log(d);
    if (!event.active) {
      this.simulation.alphaTarget(0);
    }
    d.fx = null;
    d.fy = null;
  }

  // --------      Checkbox event ----------------------
  /**
   * Listening click event on the host checkbox.
   * If checked the host option switch type style icon to health status icon.
   * If un-checked the host option switch health status icon to type style nodes.
   * @param event : event
   */
  onCheckboxHost(event: any): void {
    if (event.target.checked) {
      // checked the Host option, switch to health style nodes icon
      this.node.call(this.setNodeIMG).classed('type', false);
    } else {
      // un-checked Host option, switch to type style nodes icon
      this.node
        .attr('xlink:href', (node: INode) => {
          let mapName = '';
          if (node.stale) {
            mapName = `${node.name.toLocaleLowerCase()} stale type`;
          } else {
            mapName = `${node.name.toLocaleLowerCase()} type`;
          }
          // console.log(mapName);
          return `assets/${NodeIconMapping[mapName]}`;
        })
        .classed('type', true);
    }
  }

  /**
   * Show or hide the IP address information in each node.
   * @param event : event object.
   */
  onCheckboxIPAddr(event: any): void {
    if (event.target.checked) {
      d3.select('.ip-group').classed('hide', false);
    } else {
      d3.select('.ip-group').classed('hide', true);
    }
  }

  /**
   * Checked -> Change all Health Good nodes icons to normal icon
   * Un-checked -> Change all Health Good node icon to light color icon.
   * @param event : Event object.
   */
  onCheckboxGood(event: any): void {
    if (event.target.checked) {
      d3.selectAll('g.nodes image.good').attr('xlink:href', (node: INode) => {
        const mapName = `${node.name.toLocaleLowerCase()} good`;
        return `assets/${NodeIconMapping[mapName]}`;
      });
    } else {
      d3.selectAll('g.nodes image.good').attr('xlink:href', (node: INode) => {
        const mapName = `${node.name.toLocaleLowerCase()} good hide`;
        return `assets/${NodeIconMapping[mapName]}`;
      });
    }
  }

  /**
   * Checked -> Change all Health Warning nodes icons to normal icon.
   * Un-checked -> Change all Health Warning node icon to light color icon
   * @param event : Event object.
   */
  onCheckboxWarning(event: any): void {
    if (event.target.checked) {
      d3.selectAll('g.nodes image.warning').attr(
        'xlink:href',
        (node: INode) => {
          const mapName = `${node.name.toLocaleLowerCase()} warning`;
          return `assets/${NodeIconMapping[mapName]}`;
        }
      );
    } else {
      d3.selectAll('g.nodes image.warning').attr(
        'xlink:href',
        (node: INode) => {
          const mapName = `${node.name.toLocaleLowerCase()} warning hide`;
          return `assets/${NodeIconMapping[mapName]}`;
        }
      );
    }
  }

  /**
   * Checked -> Change all Health Critical nodes icons to normal icon.
   * Un-checked -> Change all Health Critical nodes icons to light color icon.
   * @param event : Event object.
   */
  onCheckboxCritical(event: any): void {
    if (event.target.checked) {
      d3.selectAll('g.nodes image.critical').attr(
        'xlink:href',
        (node: INode) => {
          const mapName = `${node.name.toLocaleLowerCase()} critical`;
          return `assets/${NodeIconMapping[mapName]}`;
        }
      );
    } else {
      d3.selectAll('g.nodes image.critical').attr(
        'xlink:href',
        (node: INode) => {
          const mapName = `${node.name.toLocaleLowerCase()} critical hide`;
          return `assets/${NodeIconMapping[mapName]}`;
        }
      );
    }
  }

  /**
   * Checked -> Change all Power off nodes icons to normal icon.
   * Un-checked -> Change all Power off nodes icons to light color icon.
   * @param event : Event object.
   */
  onCheckboxPowerOff(event: any): void {
    if (event.target.checked) {
      d3.selectAll('g.nodes image.off').attr('xlink:href', (node: INode) => {
        const mapName = `${node.name.toLocaleLowerCase()} off`;
        return `assets/${NodeIconMapping[mapName]}`;
      });
    } else {
      d3.selectAll('g.nodes image.off').attr('xlink:href', (node: INode) => {
        const mapName = `${node.name.toLocaleLowerCase()} off hide`;
        return `assets/${NodeIconMapping[mapName]}`;
      });
    }
  }

  /**
   * Checked -> Change all Stale nodes icons to normal icon.
   * Un-checked -> Change all Stale nodes icons to light color icon.
   * @param event : Event object.
   */
  onCheckboxStale(event: any): void {
    if (event.target.checked) {
      d3.selectAll('g.nodes image.stale').attr('xlink:href', (node: INode) => {
        const mapName = `${node.name.toLocaleLowerCase()} stale`;
        return `assets/${NodeIconMapping[mapName]}`;
      });
    } else {
      d3.selectAll('g.nodes image.stale').attr('xlink:href', (node: INode) => {
        const mapName = `${node.name.toLocaleLowerCase()} stale hide`;
        return `assets/${NodeIconMapping[mapName]}`;
      });
    }
  }

  /**
   * Checked -> Set all Link lines with status style.
   * Un-checked -> Set all Link Lines without status style.
   * @param event : Event object.
   */
  onCheckboxLinkState(event: any): void {
    if (event.target.checked) {
      // checked the Host option, switch to health style nodes icon
      d3.selectAll('.links line').classed('type-of-line', false);
      d3.selectAll('.num-of-connection-circle circle').classed(
        'type-of-line',
        false
      );
    } else {
      // un-checked Host option, switch to type style nodes icon
      d3.selectAll('.links line').classed('type-of-line', true);
      d3.selectAll('.num-of-connection-circle circle').classed(
        'type-of-line',
        true
      );
    }
  }

  /**
   * Checked -> Set all Up Link lines with status style.
   * Un-checked -> Set all Up Link Lines without status style.
   * @param event : Event object.
   */
  onCheckboxLineUp(event: any): void {
    if (event.target.checked) {
      d3.selectAll('.links line').classed('type-of-line', false);
      d3.selectAll('.num-of-connection-circle circle').classed(
        'type-of-line',
        false
      );

      // checked the Host option, switch to health style nodes icon
      d3.selectAll('.links line.up').classed('close-line-status', false);
      d3.selectAll('.num-of-connection-circle circle.up').classed(
        'close-line-status',
        false
      );
    } else {
      // un-checked Host option, switch to type style nodes icon
      d3.selectAll('.links line.up').classed('close-line-status', true);
      d3.selectAll('.num-of-connection-circle circle.up').classed(
        'close-line-status',
        true
      );
    }
  }

  /**
   * Checked -> Set all down Link lines with status style.
   * Un-checked -> Set all down Link Lines without status style.
   * @param event : Event object.
   */
  onCheckboxLineDown(event: any): void {
    if (event.target.checked) {
      d3.selectAll('.links line').classed('type-of-line', false);
      d3.selectAll('.num-of-connection-circle circle').classed(
        'type-of-line',
        false
      );

      // checked the Host option, switch to health style nodes icon
      d3.selectAll('.links line.down').classed('close-line-status', false);
      d3.selectAll('.num-of-connection-circle circle.down').classed(
        'close-line-status',
        false
      );
    } else {
      // un-checked Host option, switch to type style nodes icon
      d3.selectAll('.links line.down').classed('close-line-status', true);
      d3.selectAll('.num-of-connection-circle circle.down').classed(
        'close-line-status',
        true
      );
    }
  }

  /**
   * Checked -> Set all BMC broken Link lines with status style.
   * Un-checked -> Set all BMC broken Link Lines without status style.
   * @param event : Event object.
   */
  onCheckboxLineBMCBroken(event: any): void {
    if (event.target.checked) {
      d3.selectAll('.links line').classed('type-of-line', false);
      d3.selectAll('.num-of-connection-circle circle').classed(
        'type-of-line',
        false
      );

      // checked the Host option, switch to health style nodes icon
      d3.selectAll('.links line.bmc-broken').classed(
        'close-line-status',
        false
      );
      d3.selectAll('.num-of-connection-circle circle.bmc-broken').classed(
        'close-line-status',
        false
      );
    } else {
      // un-checked Host option, switch to type style nodes icon
      d3.selectAll('.links line.bmc-broken').classed('close-line-status', true);
      d3.selectAll('.num-of-connection-circle circle.bmc-broken').classed(
        'close-line-status',
        true
      );
    }
  }

  // ----------------- zoom in / out ------------------
  initialZoomBehavior(): void {
    this.zoom = d3
      .zoom()
      .scaleExtent([1, 4])
      .extent([
        [0, 0],
        [this.topologySectionWidth, this.topologySectionHeight],
      ])
      .on('zoom', ({ transform }) =>
        d3.select('#element-group').attr('transform', transform)
      );
  }

  /**
   * Zoom in/out the topology by slider value.
   * @param zmV : slider value. 1 ~ 4
   */
  onSliderEvent(zmV: number): void {
    this.zoom.scaleTo(
      d3.select('#element-group').transition().duration(750),
      zmV
    );
  }
}
